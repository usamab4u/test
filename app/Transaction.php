<?php

namespace App;

use  Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class Transaction
{
    static function keyName(){
        return 'transactions';
    }
    static function create($data){
        /** Validation **/
        $validator =  Validator::make($data, [
            'start_date' => 'required|date',
            'end_date' => 'required|date|after_or_equal:start_date',
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'email' => ['required', 'email'],
            'telnumber' => ['required', 'integer'],
            'address1' => ['required', 'string'],
            'address2' => [ 'string'],
            'city' => ['required', 'string'],
            'country' => ['required', 'string'],
            'postcode' => ['required', 'string'],
            'product_name' => ['required', 'string'],
            'cost' => ['required', 'numeric'],
            'currency' => ['required', 'in:usd,gbp'],

            'transaction_date' => ['required', 'date'],

        ]);
        $validator->validate();

        $trasactions = Cache::get(self::keyName(),[]);
        $id =  self::generateId();
        $trasactions[]= [
            'id'=>$id,
            'start_date' => $data['start_date'],
            'end_date'=> $data['end_date'],
            'first_name'=>$data['first_name'],
            'last_name'=>$data['last_name'],
            'email'=>$data['email'],
            'telnumber'=>$data['telnumber'],
            'address1'=>$data['address1'],
            'address2'=>$data['address2'],
            'city'=>$data['city'],
            'country'=>$data['country'],
           'postcode'=>$data['postcode'],
           'product_name'=>$data['product_name'],
           'cost'=>$data['cost'],
           'currency'=>$data['currency'],
           'transaction_date'=>$data['transaction_date']
        ];
        Cache::forever(self::keyName(),$trasactions);
        return $id;
    }
    static function get($id){
        $transactions = self::all();
        $trn =  array_search($id, array_column($transactions, 'id'));

        if($trn === false){
            throw new \Exception('Transaction not found');
        }else{
           $trn =  $transactions[$trn];
        }
        return $trn;
    }
    static function delete($id){
        $transactions = self::all();
        $trn =  array_search($id, array_column($transactions, 'id'));

        if($trn === false){
            throw new \Exception('Transaction not found');
        }
        unset($transactions[$trn]);
        Cache::forever(self::keyName(),$transactions);


        return $trn;
    }
    static function all(){
        return Cache::get(self::keyName(),[]);
    }
    static function deleteAll(){
        return Cache::forget(self::keyName());
    }

    static function count(){
       return  count(self::all());
    }

    static function generateId() {
        $number = mt_rand(0, 9999999); // better than rand()

        // call the same function if the barcode exists already
        if (self:: idExists($number)) {
            return self::generateId();
        }

        // otherwise, it's valid and can be used
        return $number;
    }

    static function idExists($id) {
        // query the database and return a boolean
        // for instance, it might look like this in Laravel
        $transactions = self::all();
        return array_search($id, array_column($transactions, 'id'));

    }
    //
}
