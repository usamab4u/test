<?php

namespace App;


class Response
{
    static function success($data){
     return [
         'type' =>'successObject',
         'data' =>$data
     ]   ;
    }
    static function successMsg($msg='Successfully'){
        return [
            'type' =>'successMsg',
            'msg' =>$msg
        ];
    }


    static function error($msg){
        return [
            'type' =>'errorMsg',
            'msg' =>$msg
        ]   ;
    }
    //
}
