<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('transaction/all',function(){
   try{
       return \App\Response::success( \App\Transaction::all());
   }catch (\Exception $ex){
       return \App\Response::error($ex->getMessage());
   }
});
Route::post('transaction/create',function(Request $request){
    try{
        \App\Transaction::create($request->all());
        return \App\Response::successMsg( );
    }catch (\Exception $ex){
        return \App\Response::error($ex->getMessage());
    }
});
Route::get('transaction/count',function(){
    try{
        return \App\Response::success(['count'=>\App\Transaction::count()] );
    }catch (\Exception $ex){
        return \App\Response::error($ex->getMessage());
    }
});
Route::get('transaction/{id}/get',function($id){
    try{
        $transaction = \App\Transaction::get($id);
        return \App\Response::success(['transaction'=>$transaction] );
    }catch (\Exception $ex){
        return \App\Response::error($ex->getMessage());
    }
});
Route::get('transaction/{id}/delete',function($id){
    try{
        $transaction = \App\Transaction::delete($id);
        return \App\Response::successMsg( );
    }catch (\Exception $ex){
        return \App\Response::error($ex->getMessage());
    }
});
Route::get('transaction/deleteAll',function(){
    try{
        \App\Transaction::deleteAll();
        return \App\Response::successMsg( );
    }catch (\Exception $ex){
        return \App\Response::error($ex->getMessage());
    }
});

