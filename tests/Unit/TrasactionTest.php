<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Transaction;
class TrasactionTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testDeleteAllTest()
    {
        Transaction::deleteAll();
        $this->assertEquals(0,Transaction::count());
    }

    public function testCreateTest()
    {
        $count = Transaction::count();
        $transaction =  Transaction::create([
            'start_date' => '6/20/2019',
            'end_date'=> '6/21/2019',
            'first_name'=>'Muhammad Usama',
            'last_name'=>'Riaz',
            'email'=>'muhammadusamariaz@gmail.com',
            'telnumber'=>123123,
            'address1'=>'123',
            'address2'=>'123ss',
            'city'=>'Lahore',
            'country'=>'Pakistan',
            'postcode'=>'54000',
            'product_name'=>'Atom',
            'cost'=>552,
            'currency'=>'usd',
            'transaction_date'=>'6/24/2019'
        ]);
        $this->assertEquals($count+1,Transaction::count());
    }
}
